# Example files for <https://pintergabor.eu/en/hw/6servo/>

Written by Pintér Gábor  
Feuerwehrgasse 1/2/9  
Purbach am Neusiedlersee  
A-7083, Austria  
Tel: +43 670 2055090  
Email: <pinter.gabor@gmx.at>  
Web: <https://pintergabor.eu>  

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

<https://www.gnu.org/copyleft/gpl.html>

For commercial license, and for custom HW, FW and SW please contact the author.

## simple - Simple blinky
Just a simple RED/GREEN blinky for the first tests.

## standard - Standard command set
Communication format is 115200 baud, no parity, one stop bit. Commands start with a command letter, and end with '\n'.

* Rxx
  Set red led intensity. xx is hexadecimal. 00 = off, FF = full on.

* Gxx
  Set green led intensity. xx is hexadecimal. 00 = off, FF = full on.

* Pnxxxx
  Set PWM for servo control. n= 1..6. xxxx = high time in milliseconds. Hexadecimal.  
  3E8 = left position, 5DC = mid position, 7D0 = right position.

* Kn
  Query key status. The answer will be Kns.  
  n = 1..3 key number. MODE = 1, UP = 2, DOWN = 3  
  s = 0..1 key status. Pressed = 1, not pressed = 0

* UUU
  Identify. And later may be used for automatic baud rate detection. Please send it first after power on.

* V
  Get version number

* \@xx
  Execute test command. Currently known commands:
  * 0 - Communication test
  * 1 - LED test
  * 2 - PWM test  

Key press and release events are reported without asking in the following format:
* kns  
  n = 1..3 key number. MODE = 1, UP = 2, DOWN = 3  
  s = 0..1 key status. Key make = 1, key break = 0

## dev - It is not four you
My development branch. Please do not touch.
